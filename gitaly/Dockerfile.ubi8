ARG CI_REGISTRY_IMAGE=
ARG TAG=

ARG GIT_IMAGE=${CI_REGISTRY_IMAGE}/git-base:${TAG}

FROM ${GIT_IMAGE}

ARG GITALY_VERSION
ARG GITLAB_USER=git

LABEL source="https://gitlab.com/gitlab-org/gitaly" \
      name="Gitaly" \
      maintainer="GitLab Distribution Team" \
      vendor="GitLab" \
      version=${GITALY_VERSION} \
      release=${GITALY_VERSION} \
      summary="Gitaly is a Git RPC service for handling all the git calls made by GitLab." \
      description="Gitaly is a Git RPC service for handling all the git calls made by GitLab."

ADD gitaly.tar.gz /
ADD gitlab-shell.tar.gz /

COPY scripts/ /scripts/
COPY config.toml /etc/gitaly/config.toml
COPY gitconfig /usr/local/etc/gitconfig

RUN dnf --disableplugin=subscription-manager install -yb --nodocs procps net-tools libicu openssh-clients \
    && adduser -m ${GITLAB_USER} \
    && mkdir -p /etc/gitaly /var/log/gitaly /home/${GITLAB_USER}/repositories \
    && touch /var/log/gitaly/gitaly.log \
    && touch /var/log/gitaly/gitlab-shell.log \
    && cp /srv/gitlab-shell/config.yml.example /etc/gitaly/shell-config.yml \
    && ln -s /etc/gitaly/shell-config.yml /srv/gitlab-shell/config.yml \
    && chown -R ${GITLAB_USER}:${GITLAB_USER} \
      /scripts \
      /etc/gitaly \
      /var/log/gitaly \
      /home/${GITLAB_USER}/repositories \
      /srv/gitaly-ruby \
      /srv/gitlab-shell \
      /etc/gitaly/shell-config.yml

USER ${GITLAB_USER}:${GITLAB_USER}

ENV CONFIG_TEMPLATE_DIRECTORY=/etc/gitaly

CMD "/scripts/process-wrapper"

VOLUME /var/log/gitaly

HEALTHCHECK --interval=30s --timeout=10s --retries=5 CMD /scripts/healthcheck
